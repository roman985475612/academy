<?php
/**
 * Template Name: Шаблон блога
 */

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col s8">
            <?php
                $posts = get_posts( array(
                    'numberposts' => 1,
                    'category'    => 0,
                    'orderby'     => 'date',
                    'order'       => 'DESC',
                    'include'     => array(),
                    'exclude'     => array(),
                    'meta_key'    => '',
                    'meta_value'  =>'',
                    'post_type'   => 'post',
                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ) );

                if ( $posts ) :

                    foreach ( $posts as $post ) : 
                        setup_postdata( $post );

                        get_template_part( 'template-parts/content', 'card' );

                    endforeach;

                    the_posts_navigation();
                    wp_reset_postdata();
                endif;
            ?>
        </div>
        <div class="col s4">
            <?php get_sidebar( 'sidebar-1' ) ?>
        </div>
    </div>
</div>
<?php get_footer() ?>