<?php

class MyWidget extends WP_Widget
{
    public function __construct()
    {
        $widget_options = [
            'classname'   => 'MyWidget',
            'description' => 'My First Widget'
        ];
        parent::__construct( 'MyWidget', 'My widget', $widget_options );
    }

    public function widget( $args, $instance )
    {
        $title = apply_filters( 'widget_title', $instance[ 'title' ] );
        $blog_title = get_bloginfo( 'name' );
        $tagline = get_bloginfo( 'description' );

        echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title']
            . '<p><strong>Site Name:</strong>' . $blog_title . '</p>'
            . '<p><strong>Tagline:</strong>' . $tagline . '</p>'
            . $args['after_widget'];
    }

    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';

        echo '<p>'
            . '<label for="' . $this->get_field_id( 'title' ) . '">Title:</label>'
            . '<input type="text" id="' . $this->get_field_id( 'title' ). '" name="' . $this->get_field_name( 'title' ) . '" value="' . esc_attr( $title ) . '" />'
            . '</p>';
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
        return $instance;
    }
}