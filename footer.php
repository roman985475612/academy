
    <!-- Footer -->
    <footer class="page-footer deep-purple lighten-1">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">About Us</h5>
              <p class="grey-text text-lighten-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque facere voluptatem magnam quo quibusdam mollitia.</p>
            </div>
            <div class="col l4 offset-l2 s12">
              <h5 class="white-text">Links</h5>
              <ul>
                <li><a class="grey-text text-lighten-3" href="index.html">Home</a></li>
                <li><a class="grey-text text-lighten-3" href="solutions.html">Solutions</a></li>
                <li><a class="grey-text text-lighten-3" href="signup.html">Sign up</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright deep-purple darken-1">
          <div class="container">
          Quazzu &copy; 2021
          <a class="grey-text text-lighten-4 right" href="#!">Terms & Conditions</a>
          </div>
        </div>
    </footer>

    <!-- Login modal -->
    <div id="modal-login" class="modal">
        <div class="modal-content">
            <h4>Account Login</h4>
            <p>Login to access your account dashboard</p>
            <form action="">
                <div class="input-field">
                    <input type="email" id="email">
                    <label for="email">Email</label>
                </div>
                <div class="input-field">
                    <input type="password" id="password">
                    <label for="password">Password</label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close btn grey">
                <i class="fa fa-sync"></i>
                Reset password
            </a>
            <a href="#!" class="modal-action modal-close btn purple">
                <i class="fa fa-lock"></i>
                Login
            </a>
        </div>
    </div>

<?php wp_footer(); ?>

</body>
</html>
