<div class="showcase container">
    <div class="row">
        <div class="col s12 m10 offset-m1 center">
            <h5>Welcome To Quazze</h5>
            <h1><?php the_title() ?></h1>
        </div>
    </div>
</div>
