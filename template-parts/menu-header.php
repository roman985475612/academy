<nav class="transparent">
    <div class="container">
        <div class="nav-wrapper">
            <a href="<?= home_url() ?>" class="brand-logo"><?php bloginfo() ?></a>
            <a href="#" data-target="mobile-menu" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <?php
                $locations = get_nav_menu_locations();
                $menu_items = wp_get_nav_menu_items( $locations[ 'menu-1' ], ['custom' => true] );
                // dd( $menu_items );
            ?>
            
            <ul id="header-menu" class="right hide-on-med-and-down">
                <?php foreach ( $menu_items as $key => $item ): ?>
                    <?php if ( isset( $item['subitems'] ) ): ?>
                        <li>
                            <a class="dropdown-trigger" href="#!" data-target="dropdown<?= $key ?>"><?= $item['title'] ?><i class="material-icons right">arrow_drop_down</i></a>
                            <ul id="dropdown<?= $key ?>" class="dropdown-content">
                                <?php foreach ( $item['subitems'] as $subitem ): ?>
                                    <li><a href="<?= $subitem['url'] ?>"><?= $subitem['title'] ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li><a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</nav>
