
<article class="card large purple white-text">
    <div class="card-image waves-effect waves-block waves-light">
        <?php the_post_thumbnail( 'large' ) ?>
    </div>
    <div class="card-content">
        <span class="card-title"><?php the_title() ?></span>
        <?php the_excerpt() ?>
    </div>
    <div class="card-action">
        <a href="<?php the_permalink() ?>"><?php _e( 'Read more', 'academy' )?></a>
    </div>
</article>
            