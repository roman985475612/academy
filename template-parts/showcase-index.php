<div class="showcase container">
    <div class="row">
        <div class="col s12 m10 offset-m1 center">
            <h5>Welcome To Quazze</h5>
            <h1>Build For The Future</h1>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio ut qui, provident maiores iure pariatur.</p>
            <br><br>
            <a href="solutions.html" class="btn btn-large white purple-text">Learn More</a>
            <a href="signup.html" class="btn btn-large purple white-text">Sign up</a>
        </div>
    </div>
</div>
