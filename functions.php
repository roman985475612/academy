<?php
/**
 * academy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package academy
 */

function dd( $data )
{
    echo '<pre style="background-color:black;color:green;padding:1rem;line-height:1.5;">' . print_r( $data, 1 ) . '</pre>';
}

function filter_menu_header( $items, $menu, $args )
{
    if ( isset( $args['custom'] ) && $args['custom'] ) {
        $result = [];
        foreach ( $items as $item ) {
            $new_item = [
                'title' => $item->title,
                'url'   => $item->url,
            ];
            if ( $item->menu_item_parent ) {
                $result[$item->menu_item_parent]['subitems'][] = $new_item;
                
            } else {
                $result[$item->ID] = $new_item;
            }
        }
        return $result;
    }
    return $items;
}
add_filter( 'wp_get_nav_menu_items', 'filter_menu_header', 10, 3 );

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'academy_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function academy_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on academy, use a find and replace
		 * to change 'academy' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'academy', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'academy' ),
				'menu-2' => esc_html__( 'Mobile', 'academy' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'academy_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'academy_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function academy_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'academy_content_width', 640 );
}
add_action( 'after_setup_theme', 'academy_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function academy_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'academy' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'academy' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'academy_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function academy_scripts() {
	// wp_enqueue_style( 'academy-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'academy-style', 'rtl', 'replace' );

	wp_enqueue_style( 'academy-googlefonts-style', 'https://fonts.googleapis.com/icon?family=Material+Icons', array(), _S_VERSION );
	wp_enqueue_style( 'academy-fontawesome-style', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'academy-materialize-style', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'academy-main-style', get_template_directory_uri() . '/assets/css/main.css', array(), _S_VERSION );

    wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.2.1.min.js');
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'academy-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'academy-materialize-script', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'academy-main-script', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'academy_scripts' );

require get_template_directory() . '/MyWidget.php';

function register_my_widgets()
{
	register_widget( 'MyWidget' );
}
add_action( 'widgets_init', 'register_my_widgets' );

add_filter( 'excerpt_length', function() {
	return 40;
} );

add_filter( 'excerpt_more', function( $more ) {
	return '...';
});

function my_navigation_template( $template, $class )
{
	return '<nav class="navigation %1$s" role="navigation">%3$s</nav>';
}
add_filter( 'navigation_markup_template', 'my_navigation_template', 10, 2 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
