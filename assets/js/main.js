$(document).ready(function () {
    $('.sidenav').sidenav()

    $('.carousel.carousel-slider').carousel({
        duration: 100,
        fullWidth: true,
        indicators: true
    })

    $('.modal').modal()
    
    $('.dropdown-trigger').dropdown({
        gutter: ($('.dropdown-content').width() * 3) / 3.05 + 3
    })
})

document.addEventListener('DOMContentLoaded', function() {
    M.Tabs.init(document.querySelector('.tabs'))
    
    M.FormSelect.init(document.querySelectorAll('select'))
    
    let loginBtn = document.querySelector('a[href="#modal-login"]')
    loginBtn.addEventListener('click', () => {
        M.Modal.getInstance(document.getElementById('modal-login')).open()
    })

    let triggers = document.querySelectorAll('.dropdown-trigger')
    triggers.forEach ( trigger => {
        let target = trigger.dataset.target
        let dropdown = trigger.nextElementSibling
        dropdown.setAttribute('id', target)
    })

})

// Active link    
let path = window.location.origin + window.location.pathname

let headerMenuItems = document.querySelectorAll( '#header-menu > li' )
    
for (i = 0; i < headerMenuItems.length; i++ ) {
    $li = headerMenuItems[i]
    if (path === $li.childNodes[0].href) {
        $li.classList.add( 'active-link' )
    }    
}