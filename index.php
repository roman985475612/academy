<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package academy
 */

get_header();
?>
	<main class="container">
		<div class="row">
			<div class="col m8">
				<?php
					if ( have_posts() ) :

						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'card' );

						endwhile;
						
						$args = [
							'base'         => '%_%',
							'format'       => '?page=%#%',
							'total'        => 10,
							'current'      => 0,
							'show_all'     => False,
							'end_size'     => 1,
							'mid_size'     => 2,
							'prev_next'    => True,
							'prev_text'    => __('« Previous'),
							'next_text'    => __('Next »'),
							'type'         => 'plain',
							'add_args'     => False,
							'add_fragment' => '',
							'before_page_number' => '',
							'after_page_number'  => ''
						];
						
						echo paginate_links( $args );

						// the_posts_pagination([
						// 	'end_size' => 1,
						// 	'mid_size' => 1,
						// 	'type'     => 'list'
						// ]);

					endif;
				?>

			</div>
			<div class="col m4">
				<?php get_sidebar(); ?>
			</div>
		</div>

	</main><!-- #main -->

<?php
get_footer();
